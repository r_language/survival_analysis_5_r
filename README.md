# Survival_Analysis_5_R

In this R file you can have a look at another survival analysis and you don't need data. Indeed, we use data from survival package. You can find : Kaplan Meier Estimator, Log rank test, Cox model, Log linearity test, likelihood, and step function to select the best model. Enjoy ! 

Author : Marion Estoup 

E-mail : marion_110@hotmail.fr

November 2022
